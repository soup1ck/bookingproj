package com.vsu.bookingpetproj.dto;


import lombok.Data;

import java.util.List;

@Data
public class AccountDto {

    private Long id;

    private String accountName;

    private List<BookingDto> bookings;

}
