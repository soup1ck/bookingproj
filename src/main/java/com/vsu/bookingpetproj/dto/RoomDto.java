package com.vsu.bookingpetproj.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Set;

@Data
public class RoomDto {

    private Long id;

    private String name;

    private BigDecimal price;

    private Set<BookingDto> bookings;

}
