package com.vsu.bookingpetproj.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.Set;

@Data
public class BookingDto {

    private Long id;

    private String comment;

    private LocalDate fromUtc;

    private LocalDate toUtc;

//    private AccountDto accountDto;

      private Set<RoomDto> rooms;

}
