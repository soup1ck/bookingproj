package com.vsu.bookingpetproj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookingPetProjApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookingPetProjApplication.class, args);
    }

}
