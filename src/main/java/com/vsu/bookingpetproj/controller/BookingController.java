package com.vsu.bookingpetproj.controller;

import com.vsu.bookingpetproj.dto.BookingDto;
import com.vsu.bookingpetproj.entity.Booking;
import com.vsu.bookingpetproj.service.BookingServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/booking")
public class BookingController {

    @Autowired
    private BookingServiceImpl bookingService;

    @GetMapping("/{id}")
    public BookingDto getBookingById(@PathVariable Long id) {

        return bookingService.getBookingById(id);
    }

    @GetMapping("/all")
    public List<BookingDto> getAllBookings() {

        return bookingService.getAllBookings();
    }

    @PostMapping("/add")
    public Booking addBooking(@RequestBody Booking booking) {

        return bookingService.addBooking(booking);
    }

    @DeleteMapping("/delete/{id}")
    public void delBooking(@PathVariable Long id) {

        bookingService.delBooking(id);
    }

    @PutMapping("/upd/{id}")

    public ResponseEntity<Object> updBooking(@RequestBody Booking booking,
                                             @PathVariable Long id,
                                             @RequestParam String comment,
                                             @RequestParam LocalDate fromUtc,
                                             @RequestParam LocalDate toUtc
                                             )
    {
        return bookingService.updBooking(booking,id,comment,fromUtc,toUtc);
    }
}
