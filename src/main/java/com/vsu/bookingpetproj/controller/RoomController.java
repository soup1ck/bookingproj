package com.vsu.bookingpetproj.controller;

import com.vsu.bookingpetproj.dto.RoomDto;
import com.vsu.bookingpetproj.entity.Room;
import com.vsu.bookingpetproj.service.RoomServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/room")
public class RoomController {

    @Autowired
    private RoomServiceImpl roomService;

    @GetMapping("/{id}")
    public RoomDto getRoomById(@PathVariable Long id){

        return roomService.getRoomById(id);
    }

    @GetMapping("/all")
    public List<RoomDto> getAllRooms(){

        return roomService.getAllRooms();
    }

    @PostMapping("/add")
    public Room addRoom(@RequestBody Room room){

        return roomService.addRoom(room);
    }

    @DeleteMapping("/delete/{id}")
    public void delRoom(@PathVariable Long id){

        roomService.delRoom(id);
    }

    @PutMapping("/upd/{id}")
    public ResponseEntity<Object> updRoom(@RequestBody Room room,
                                          @PathVariable Long id,
                                          @RequestParam String comment,
                                          @RequestParam BigDecimal price)
    {
       return  roomService.updRoom(room,id,comment,price);
    }
}
