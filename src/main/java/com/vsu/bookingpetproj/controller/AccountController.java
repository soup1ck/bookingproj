package com.vsu.bookingpetproj.controller;

import com.vsu.bookingpetproj.dto.AccountDto;
import com.vsu.bookingpetproj.entity.Account;
import com.vsu.bookingpetproj.service.AccountServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountServiceImpl accountService;

    @GetMapping("/all")
    public List<AccountDto> getAllAccounts() {

        return accountService.getAllAccounts();
    }

    @GetMapping("/{id}")
    public AccountDto getAccountById(@PathVariable Long id) {

        return accountService.getAccountById(id);
    }

    @GetMapping("/name/{accountName}")
    public AccountDto getAccountByAccountName(@PathVariable String accountName) {

        return accountService.getAccountByAccountName(accountName);
    }

    @PostMapping("/add")
    public Account addPeople(@RequestBody Account account) {

        return accountService.addAccount(account);
    }

    @DeleteMapping("/delete/{id}")
    public void delAccount(@PathVariable Long id) {

        accountService.delAccount(id);
    }

    @PutMapping("/upd/{id}")
    public ResponseEntity<Object> updAccount(@RequestBody Account account,
                                             @PathVariable Long id,
                                             @RequestParam String name)
    {
        return accountService.updAccount(account, id, name);
    }
}
