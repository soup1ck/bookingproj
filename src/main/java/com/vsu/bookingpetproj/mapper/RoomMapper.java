package com.vsu.bookingpetproj.mapper;

import com.vsu.bookingpetproj.dto.RoomDto;
import com.vsu.bookingpetproj.entity.Room;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoomMapper {

    RoomDto fromRoom (Room room);
}
