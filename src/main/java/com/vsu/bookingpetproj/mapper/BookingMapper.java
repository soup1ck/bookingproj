package com.vsu.bookingpetproj.mapper;

import com.vsu.bookingpetproj.dto.BookingDto;
import com.vsu.bookingpetproj.entity.Booking;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BookingMapper {

    BookingDto fromBooking (Booking booking);
}
