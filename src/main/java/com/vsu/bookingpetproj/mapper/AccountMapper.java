package com.vsu.bookingpetproj.mapper;

import com.vsu.bookingpetproj.dto.AccountDto;
import com.vsu.bookingpetproj.entity.Account;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface AccountMapper {

    AccountDto fromAccount(Account account);
}
