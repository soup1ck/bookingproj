package com.vsu.bookingpetproj.service;

import com.vsu.bookingpetproj.dto.BookingDto;
import com.vsu.bookingpetproj.entity.Booking;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.List;

public interface BookingService {

    BookingDto getBookingById(Long id);

    List<BookingDto> getAllBookings();

    Booking addBooking (Booking booking);

    void delBooking(Long id);

    ResponseEntity<Object> updBooking(Booking booking, Long id, String comment, LocalDate fromUtc, LocalDate toUtc);
}
