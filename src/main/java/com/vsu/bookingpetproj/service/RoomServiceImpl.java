package com.vsu.bookingpetproj.service;

import com.vsu.bookingpetproj.dto.RoomDto;
import com.vsu.bookingpetproj.entity.Room;
import com.vsu.bookingpetproj.mapper.RoomMapper;
import com.vsu.bookingpetproj.repository.RoomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class RoomServiceImpl implements RoomService{

    private final RoomRepository roomRepository;
    private final RoomMapper roomMapper;

    @Override
    public RoomDto getRoomById(Long id) {
        Optional<Room> roomOptional = roomRepository.findById(id);
        if(roomOptional.isEmpty()){
            return null;
        }
        Room room = roomOptional.get();
        RoomDto roomDto = roomMapper.fromRoom(room);
        return roomDto;
    }

    @Override
    public List<RoomDto> getAllRooms() {
        List<RoomDto> roomDtoList= new ArrayList<>();
        List<Room> roomList = roomRepository.findAll();
        for(Room room:roomList){
            roomDtoList.add(roomMapper.fromRoom(room));
        }
        return  roomDtoList;
    }

    @Override
    public Room addRoom(Room room) {
        return roomRepository.save(room);
    }

    @Override
    public void delRoom(Long id) {
        roomRepository.deleteById(id);
    }

    @Override
    public ResponseEntity<Object> updRoom(Room room,Long id, String name, BigDecimal price) {
        Optional<Room> roomOptional = roomRepository.findById(id);
        if(roomOptional.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        room.setName(name);
        room.setPrice(price);
        roomRepository.save(room);
        return ResponseEntity.noContent().build();
    }
}
