package com.vsu.bookingpetproj.service;

import com.vsu.bookingpetproj.dto.BookingDto;
import com.vsu.bookingpetproj.entity.Booking;
import com.vsu.bookingpetproj.mapper.BookingMapper;
import com.vsu.bookingpetproj.repository.BookingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class BookingServiceImpl implements BookingService {

    private final BookingRepository bookingRepository;
    private final BookingMapper bookingMapper;

    @Override
    public BookingDto getBookingById(Long id) {
        Optional<Booking> bookingOptional = bookingRepository.findById(id);
        if (bookingOptional.isEmpty()) {
            return null;
        }
        Booking booking = bookingOptional.get();
        BookingDto bookingDto = bookingMapper.fromBooking(booking);
        return bookingDto;
    }

    @Override
    public List<BookingDto> getAllBookings() {
        List<BookingDto> bookingDtoList = new ArrayList<>();
        List<Booking> bookingList = bookingRepository.findAll();
        for (Booking booking : bookingList) {
            bookingDtoList.add(bookingMapper.fromBooking(booking));
        }
        return bookingDtoList;
    }

    @Override
    public Booking addBooking(Booking booking) {

        return bookingRepository.save(booking);
    }

    @Override
    public void delBooking(Long id) {

        bookingRepository.deleteById(id);
    }

    @Override
    public ResponseEntity<Object> updBooking(Booking booking, Long id, String comment, LocalDate fromUtc, LocalDate toUtc) {

        Optional<Booking> bookingOptional = bookingRepository.findById(id);
        if (bookingOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        booking.setComment(comment);
        booking.setFromUtc(fromUtc);
        booking.setToUtc(toUtc);
        bookingRepository.save(booking);
        return ResponseEntity.noContent().build();
    }
}
