package com.vsu.bookingpetproj.service;

import com.vsu.bookingpetproj.dto.RoomDto;
import com.vsu.bookingpetproj.entity.Room;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.List;

public interface RoomService {

    RoomDto getRoomById(Long id);

    List<RoomDto> getAllRooms();

    Room addRoom(Room room);

    void delRoom(Long id);

    ResponseEntity<Object> updRoom(Room room,Long id, String name, BigDecimal price);
}
