package com.vsu.bookingpetproj.service;

import com.vsu.bookingpetproj.dto.AccountDto;
import com.vsu.bookingpetproj.entity.Account;
import com.vsu.bookingpetproj.mapper.AccountMapper;
import com.vsu.bookingpetproj.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;

    @Override
    public AccountDto getAccountById(Long id) {

        Optional<Account> accountOptional =accountRepository.findById(id);
        if (accountOptional.isEmpty()){
            return null;
        }
        Account account = accountOptional.get();
        AccountDto accountDto = accountMapper.fromAccount(account);
        return accountDto;
    }

    @Override
    public List<AccountDto> getAllAccounts() {

        List<AccountDto> accountDtoList = new ArrayList<>();
        List<Account> accountList = accountRepository.findAll();
        for(Account account: accountList){
            accountDtoList.add(accountMapper.fromAccount(account));
        }
        return accountDtoList;
    }

    @Override
    public AccountDto getAccountByAccountName(String accountName) {
        Optional<Account> accountOptional =accountRepository.findAccountByAccountName(accountName);
        if (accountOptional.isEmpty()){
            return null;
        }
        Account account = accountOptional.get();
        AccountDto accountDto = accountMapper.fromAccount(account);
        return accountDto;
    }

    @Override
    public Account addAccount(Account account) {

        return accountRepository.save(account);
    }

    @Override
    public void delAccount(Long id) {

        accountRepository.deleteById(id);
    }

    @Override
    public ResponseEntity<Object> updAccount(Account account,Long id,String name) {

        Optional<Account> accountOptional =accountRepository.findById(id);
        if(accountOptional.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        account.setAccountName(name);
        accountRepository.save(account);
        return ResponseEntity.noContent().build();
    }

}
