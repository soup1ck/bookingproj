package com.vsu.bookingpetproj.service;

import com.vsu.bookingpetproj.dto.AccountDto;
import com.vsu.bookingpetproj.entity.Account;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AccountService {

    AccountDto getAccountById(Long id);

    List<AccountDto> getAllAccounts();

    AccountDto getAccountByAccountName(String accountName);

    Account addAccount(Account account);

    void delAccount(Long id);

    ResponseEntity<Object> updAccount(Account account,Long id,String name);
}
