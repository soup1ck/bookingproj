package com.vsu.bookingpetproj.repository;

import com.vsu.bookingpetproj.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepository extends JpaRepository<Booking,Long> {
}
