package com.vsu.bookingpetproj.repository;

import com.vsu.bookingpetproj.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomRepository extends JpaRepository<Room,Long> {
}
