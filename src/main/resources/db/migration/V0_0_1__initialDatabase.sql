CREATE TABLE account
(
    id           SERIAL NOT NULL,
    account_name VARCHAR(255),
    CONSTRAINT pk_account PRIMARY KEY (id)
);
CREATE TABLE booking
(
    id         SERIAL NOT NULL,
    comment    VARCHAR(255),
    from_utc   date,
    to_utc     date,
    account_id BIGINT,
    CONSTRAINT pk_booking PRIMARY KEY (id)
);

ALTER TABLE booking
    ADD CONSTRAINT FK_BOOKING_ON_ACCOUNT FOREIGN KEY (account_id) REFERENCES account (id);
CREATE TABLE room
(
    id    SERIAL NOT NULL,
    name  VARCHAR(255),
    price DECIMAL,
    CONSTRAINT pk_room PRIMARY KEY (id)
);
CREATE TABLE booking_room
(
    booking_id BIGINT NOT NULL,
    room_id    BIGINT NOT NULL,
    CONSTRAINT pk_booking_room PRIMARY KEY (booking_id, room_id)
);

ALTER TABLE booking_room
    ADD CONSTRAINT fk_booroo_on_booking FOREIGN KEY (booking_id) REFERENCES booking (id);

ALTER TABLE booking_room
    ADD CONSTRAINT fk_booroo_on_room FOREIGN KEY (room_id) REFERENCES room (id);